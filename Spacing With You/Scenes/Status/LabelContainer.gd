extends VBoxContainer

func _ready():
	get_node("Oxygen")			.text = "Oxygen         : " + str(Global.oxygen)
	get_node("Food")			.text = "Food           : " + str(Global.food)
	get_node("Fuel")			.text = "Fuel           : " + str(Global.fuel)
	get_node("Surface Probe")	.text = "Surface Probe  : " + str(Global.surface_probe)
	
