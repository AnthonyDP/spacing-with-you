extends Area2D

export(String) var modal_object
var mouse_in = false
func _input(event):
	if mouse_in == true:
		if event.is_pressed() && self.get_parent().get_node("Control2").visible == false:
			self.get_node("Control/ColorRect/Close Map").visible = true
			self.get_node("Control/ColorRect/Open Map").visible = false
			self.get_parent().get_node("Control2").visible = true
		elif event.is_pressed() && self.get_parent().get_node("Control2").visible == true:
			self.get_node("Control/ColorRect/Close Map").visible = false
			self.get_node("Control/ColorRect/Open Map").visible = true
			self.get_parent().get_node("Control2").visible = false



func _on_Area2D_mouse_entered():
	mouse_in = true
	print("mouse entered")
	


func _on_Area2D_mouse_exited():
	mouse_in = false # Replace with function body.
	print("mouse exited")
