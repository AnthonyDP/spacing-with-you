extends Area2D

var mouse_in = false

func _input(event):
	if mouse_in == true:
		if event.is_pressed() && self.get_parent().get_parent().get_node("Are You Sure Prompt").visible == true:
			self.get_parent().get_parent().get_node("Open Map").visible = true
			self.get_parent().get_parent().get_node("Open Map").get_node("ColorRect").get_node(self.get_parent().destination).get_node("SetWaypoint").visible = true
			self.get_parent().get_parent().get_node("Open Map").get_node("ColorRect").get_node(Global.on_planet).get_node("SetWaypoint").visible = false
			self.get_parent().get_parent().get_node("Are You Sure Prompt").visible = false
			if !(Global.is_on_the_way()):
				Global.set_on_planet(self.get_parent().destination)
			

func _on_Area2D_mouse_entered():
	mouse_in = true
	print("mouse entered")
	

func _on_Area2D_mouse_exited():
	mouse_in = false # Replace with function body.
	print("mouse exited")