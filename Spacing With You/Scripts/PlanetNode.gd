extends Area2D

export(String) var destination
export(int) var time_to_destination


var mouse_in = false

func _input(event):
	if mouse_in == true:
		if Global.on_planet != destination:
			if event.is_pressed() && self.get_parent().get_parent().get_parent().get_parent().get_node("Are You Sure Prompt").visible == false :
				self.get_parent().get_parent().get_parent().get_parent().get_node("Are You Sure Prompt").change_text(destination, time_to_destination)
				self.get_parent().get_parent().get_parent().get_parent().get_node("Are You Sure Prompt").visible = true
				self.get_parent().get_parent().get_parent().get_parent().get_node("Open Map").visible = false
				
			elif event.is_pressed() && self.get_parent().get_parent().get_parent().get_node("Control2").visible == true:
				self.get_parent().get_parent().get_parent().get_parent().get_node("Are You Sure Prompt").visible = false
				self.get_parent().get_parent().get_parent().get_parent().get_node("Open Map").visible = true
		else:
			print("Play sound no-no beep")
			



func _on_Area2D_mouse_entered():
	mouse_in = true
	print("mouse entered")
	
func _on_Area2D_mouse_exited():
	mouse_in = false # Replace with function body.
	print("mouse exited")