extends Node2D

export(String) var on_planet = "Tempest"
var on_the_way = false
var timer = 0
var countdown = false
var resource_string = "None"
var mikeSpeaking = false

export(int) var oxygen = 100
export(int) var food = 100
export(int) var fuel = 100
export(int) var surface_probe = 3

var systemSpeaking = false
var alienSpeaking = false
var inChoices = false
var chapter = "prolog"
var bondingPoin = 0
var click = 0
signal on_planet_change

func set_on_planet(on_planet):
	self.on_planet = on_planet
	emit_signal("on_planet_change")

func arrival(timer):
	self.countdown = true
	self.timer= timer
	
func is_on_the_way():
	return self.on_the_way
func arrived():
	self.on_the_way = false
	
func _countdown():
	if self.countdown:
		timer -= 1
		if timer == 0:
			countdown = false

func calculate_resource_consumption(timer):
	return self.resource_string