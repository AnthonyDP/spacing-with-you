extends Node2D

export (String) var planet_name = "Earth"
export (String) var planet_texture = "TerranPlanet"
export (int) var temperature = 0
export (String, "breathable clean", "breathable polluted", "non breathable") var atmosphere = "breathable clean"
export (int) var population = 0
export (Array, String, "test","fuel", "dangerous plant", "dangerous animal", "metal", "surface probe", "edible animals", "edible plants", "repair kit", "health kit") var resources = []
export (String) var description = "lorem ip sum"

func _ready():
	Global.connect("on_planet_change", self, "set_visibility")
	get_node("Planet_texture").planet_texture_loader(planet_texture)
	get_node("Planet_info/text_info").text_setter(planet_name, temperature, atmosphere, population, resources, description)

func _on_ShowInfoButton_pressed():
	get_node("Planet_info").visible = true

func _on_ExitButton_pressed():
		get_node("Planet_info").visible = false
		
func set_visibility():
	if (Global.on_planet == planet_name):
		$".".visible = true
	else:
		$".".visible = false
		get_node("Planet_info").visible = false
		
