extends Sprite

func planet_texture_loader(planet_name):
	var planet_texture = load("res://Assets/Planets/"+ planet_name + ".png")
	$".".set_texture(planet_texture)