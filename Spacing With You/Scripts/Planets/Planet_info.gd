extends RichTextLabel

func text_setter(planet_name, temperature, atmosphere, population, resources, description):
	$".".clear()
	$".".add_text(planet_name + " Planet")
	$".".add_text("\n\nTemperature : \n	>	" + str(temperature))
	$".".add_text("\n\nAtmosphere : \n	>	" + atmosphere)
	$".".add_text("\n\nPopulation : \n	>	" + str(population))
	$".".add_text("\n\nResources :")
	for resource in resources:
		$".".add_text("\n	>	" + resource) 
	$".".add_text("\n\nDescription	: \n	>	" + description)