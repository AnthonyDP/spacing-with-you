extends Control

var destination = ""
var time_to_destination = 0
func change_text(destination, time_to_destination):
	set_destination(destination)
	self.get_node("Are You Sure").text = "Travel to Planet " + self.destination \
	 + "? You cannot change your destination once you are on your way." \
	 + "\n\nResources that will be spent: " + Global.calculate_resource_consumption(self.destination)

func set_destination(destination):
	self.destination = destination
func set_time_to_destination(time_to_destination):
	self.time_to_destination = time_to_destination